FROM openjdk:17-alpine

EXPOSE 8080

CMD mkdir extractor

COPY ./target/uf-csv-0.0.1-SNAPSHOT.jar extractor/uf.jar

CMD ping localhost
