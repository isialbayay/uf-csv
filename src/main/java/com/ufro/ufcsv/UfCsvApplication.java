package com.ufro.ufcsv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UfCsvApplication {

	public static void main(String[] args) {
		SpringApplication.run(UfCsvApplication.class, args);
	}

}
