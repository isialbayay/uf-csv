package com.ufro.ufcsv.utils;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.ufro.ufcsv.model.Uf;

import java.io.FileReader;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Extractor {

    private static ArrayList<Uf> ufs = new ArrayList<>();
    public static void readDataFromCustomSeparator()
    {
        String file = "./src/main/resources/uf.csv";
        try {
            FileReader filereader = new FileReader(file);

            CSVParser parser = new CSVParserBuilder().withSeparator(',').build();

            CSVReader csvReader = new CSVReaderBuilder(filereader)
                    .withCSVParser(parser)
                    .build();

            List<String[]> allData = csvReader.readAll();
            for (String[] row : allData) {
                if(row[1].compareTo("UF")==0){
                    continue;
                }
                String fecha = "";
                String valor = "";
                int aux = 1;
                for (String cell : row) {
                    if (aux == 1) {
                        fecha=cell;
                    }
                    if(aux==2){
                        valor=cell;
                    }
                    aux++;
                }
                if(aux==2){
                    aux=1;
                }
                Uf uf = new Uf(formatoFecha(fecha),formatoValor(valor));
                ufs.add(uf);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static LocalDate formatoFecha(String fecha){
        String[] strings=fecha.split("/");
        int mes=Integer.parseInt(strings[0]);
        int dia=Integer.parseInt(strings[1]);
        int año=Integer.parseInt(strings[2]);

        if(año<21 && año>-1){
            año=año+2000;
        }
        else{
            año=año+1900;
        }

        return LocalDate.of(año,mes,dia);
    }

    public static double formatoValor(String valor) {
        valor = valor.replaceAll(",", "");
        return Double.parseDouble(valor);
    }

    public static double calcularPromedioMes(int mes,int año){
        double valor=0;
        int contador=0;

        for (Uf uf : ufs) {
            if(uf.getFecha().getYear()==año && uf.getFecha().getMonthValue()==mes){
                valor=valor+uf.getValor();
                contador++;
            }
        }

        return valor/contador;
    }

    public static double calcularPromedioAño(int año){
        double valor=0;
        int contador=0;
        for(int i=0; i<12; i++){
            valor=valor+calcularPromedioMes((i+1),año);
            contador++;
        }
        return valor/contador;
    }

    public static double calcularVariacion(String fecha1, String fecha2){
        double v1=0;
        double v2=0;
        for (Uf uf:ufs) {
            if(uf.getFecha().toString().equals(fecha1)){
                v1=uf.getValor();
            }
            if (uf.getFecha().toString().equals(fecha2)){
                v2=uf.getValor();
            }
        }

        return (v2-v1)/v1*100;
    }


}
