package com.ufro.ufcsv.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ufro.ufcsv.utils.Extractor;

import java.io.IOException;

@RestController
public class UfController {

    @GetMapping("uf_agosto1999")
    public String UFAgosto() throws IOException {
        Extractor.readDataFromCustomSeparator();
        return "El valor promedio de la Uf en agosto de 1999 es: $"+ Extractor.calcularPromedioMes(8,1999);
    }

    @GetMapping("uf_año2010")
    public String UFAño2010() throws IOException {

        Extractor.readDataFromCustomSeparator();
        String texto = "<h2> UF AÑO 2010 </h2> \n";
        for (int i=0; i<12; i++){
            texto = texto + "<br> Promedio UF Mes "+(i+1)+ ": $" +Extractor.calcularPromedioMes(i+1,2010);
        }

        texto = texto + "<br> El valor promedio de la UF (TOTAL) el año 2010 es:$ " + Extractor.calcularPromedioAño(2010);

        return texto;
    }

    @GetMapping("uf_variacion")
    public String variacionUF() throws IOException {
        Extractor.readDataFromCustomSeparator();
        return " El porcentaje de variación de la UF entre el 01/01/1998 y el 01/01/2000 es: %" + Extractor.calcularVariacion("1998-01-01","2000-01-01");

    }

}
