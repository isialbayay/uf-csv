package com.ufro.ufcsv.model;

import org.springframework.cglib.core.Local;

import java.time.LocalDate;
import java.util.Date;

public class Uf {

    private LocalDate fecha;
    private double valor;

    public Uf(LocalDate fecha, double valor) {
        this.fecha = fecha;
        this.valor = valor;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
}
